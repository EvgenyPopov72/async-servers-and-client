import asyncio
import logging
import time

import async_timeout
from aiohttp import web, ClientSession

TIMEOUT = 2
logger = logging.getLogger('Receiver server')
logging.basicConfig(level='DEBUG')


async def get(url, session):
    t0 = time.time()
    try:
        with async_timeout.timeout(TIMEOUT):
            async with session.get(url) as response:
                status, data = response.status, await response.json()
                logger.debug(status, data)
    except asyncio.TimeoutError:
        logger.debug('Timeout')
        data = {'server': url, 'status': 'timeout'}
    elapsed = time.time() - t0
    logger.debug('Request completed in {:.2f}s'.format(elapsed))
    return data


async def get_data(request):
    async with ClientSession() as session:
        urls = (f'http://localhost:{p}/' for p in range(8081, 8084))
        tasks = [get(url, session) for url in urls]
        data = await asyncio.gather(*tasks)
        logger.debug('All done')
        result = []
        [result.extend(res) for res in data if isinstance(res, list)]
        logger.debug(result)
    # Сортируем результаты запросов по `id` в порядке возрастания
    return web.json_response(sorted(result, key=lambda x: x.get('id', -1)))


app = web.Application()
app.add_routes([web.get('/', get_data)])
web.run_app(app, port=8080)
