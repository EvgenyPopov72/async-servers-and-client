import time

from sources_servers import get_apps, SERVER_DATA
from mock import patch


@patch('random.uniform', lambda *_, **__: 0)
class TestSources:

    async def test_ok(self, aiohttp_client):
        for idx, (app, _) in enumerate(get_apps(), start=1):
            client = await aiohttp_client(app)
            time_start = time.time()
            resp = await client.get('/')
            time_end = time.time()
            print('Elapsed: %s' % ((time_end - time_start) * 1000))
            assert resp.status == 200
            json = await resp.json()
            range1, range2 = SERVER_DATA[idx]
            ids = [i for i in range(*range1)] + [i for i in range(*range2)]
            expected_json = [
                {"server": idx, "id": id, "name": f"Test {id}"} for id in ids
            ]
            assert json == expected_json
