import asyncio
import logging
import random
from functools import wraps, partial

from aiohttp import web

SERVER_DATA = {
    1: [(1, 11), (31, 41)],
    2: [(11, 21), (41, 51)],
    3: [(21, 31), (51, 61)],
}

logger = logging.getLogger('Sources server')
logging.basicConfig(level='DEBUG')

def delayit(arg=None, min=0, max=2):
    def delayit_deco(func, timeout_func):
        @wraps(func)
        async def wrapper(*args, **params):
            # Имитируем сетевые задержки
            timeout = timeout_func()
            await asyncio.sleep(timeout)
            return await func(*args, **params)

        return wrapper

    if callable(arg):
        timeout_func = partial(random.uniform, min, max)
        return delayit_deco(arg, timeout_func=timeout_func)
    elif isinstance(arg, float):
        timeout_func = lambda: arg
    else:
        timeout_func = lambda: 0

    return partial(delayit_deco, timeout_func=timeout_func)


def handler_factory(server_num, ranges):
    @delayit
    async def handler(request):
        ids = []
        for rg in ranges:
            ids.extend([i for i in range(*rg)])

        data = [{
            "server": server_num,
            "id": id,
            "name": f"Test {id}"
        } for id in ids]
        return web.json_response(data)

    return handler

async def start_server(app, host='127.0.0.1', port=8080):
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, host, port)
    await site.start()
    logger.info('Running on http://{host}:{port}', dict(host=host, port=port))


async def start_servers(apps_ports):
    [await start_server(app, port=port) for app, port in apps_ports]


def get_apps():
    apps = []
    for server_num, id_ranges in SERVER_DATA.items():
        handler = handler_factory(server_num, id_ranges)
        app = web.Application()
        app.add_routes([
            web.get('/', handler)
        ])
        apps.append((app, 8080 + server_num))

    return apps


def run_server():
    loop = asyncio.get_event_loop()

    loop.run_until_complete(start_servers(get_apps()))
    logger.info('(Press CTRL+C to quit)')

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logger.info('Stopping...')
        loop.close()


if __name__ == '__main__':
    run_server()
